# Copyright 2009 Ingmar Vanhassel
# Copyright 2010 Bo Ørsted Andresen
# Copyright 2015, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=${PNV//./-} ] cmake
require wxwidgets [ with_opt=true option_name=gui ]
require alternatives

SUMMARY="A chemical toolbox designed to speak the many languages of chemical data"
DESCRIPTION="
Open Babel is a community-driven scientific project including both
cross-platform programs and a developer library designed to support molecular
modeling, chemistry, and many related areas, including interconversion of file
formats and data.
"
HOMEPAGE="http://openbabel.org/ ${HOMEPAGE}"

LICENCES="GPL-2"
SLOT="3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    coordgen [[ description = [ Support for 2D coordinate generation ] ]]
    doc
    gui    [[ description = [ Build a GUI using wxWidgets ] ]]
    java   [[ description = [ Adds bindings for Java ] ]]
    json   [[ description = [ Support for json based file formats ] ]]
    maestro [[ description = [ Support for the Schrödinger Maestro file format ] ]]
    openmp [[ description = [ Enable parallel processing using OpenMP ] ]]
    R      [[ description = [ Adds bindings for R ] ]]
    perl python"

DEPENDENCIES="
    build:
        sci-libs/eigen:3[>=2.91.0]
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/boost[>=1.45.0]
        dev-libs/libxml2:2.0
        sci-chemistry/inchi
        sys-libs/zlib
        x11-libs/cairo   [[ note = [ PNG output ] ]]
        coordgen? ( sci-chemistry/coordgenlibs )
        java? (
            dev-lang/swig[>=2.0]
            virtual/jdk:=   [[ note  = [ Java Native Interface ] ]]
        )
        json? ( dev-libs/rapidjson )
        maestro? ( sci-chemistry/maeparser )
        openmp? ( sys-libs/libgomp:= )
        perl? (
            dev-lang/perl:=
            dev-lang/swig[>=2.0]
        )
        python? (
            dev-lang/python:=
            dev-lang/swig[>=2.0]
        )
        R? (
            dev-lang/swig[>=2.0]
            sci-lang/R
        )
    run:
        !sci-chemistry/openbabel:0[<2.4.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
    test:
        dev-lang/python:*
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Fix-test-failure-with-Python-3.patch
    "${FILES}"/${PN}-Use-GnuInstallDirs-to-install-data-files.patch
    "${FILES}"/${PN}-Include-ctime-for-clock.patch
)

# Skip two failing tests (out of 220)
DEFAULT_SRC_TEST_PARAMS=(
    ARGS+="-E '(test_tautomer_22|test_tautomer_27)'"
)

src_configure() {
    local cmakeparams=(
        -DBABEL_DATADIR:PATH=/usr/share/${PN}
        -DBINDINGS_ONLY:BOOL=FALSE
        -DBUILD_EXAMPLES:BOOL=FALSE
        -DBUILD_SHARED:BOOL=TRUE
        -DCSHARP_BINDINGS:BOOL=FALSE
        # We need this atm because in a previous version libinchi was bundled.
        # So there is still another version of libinchi installed before openbabel
        # is upgraded and we need to make sure to build against the right one.
        -DINCHI_INCLUDE_DIR=/usr/$(exhost --target)/inchi
        -DINCHI_LIBRARY=/usr/$(exhost --target)/lib/libinchi.so.1
        -DOPENBABEL_USE_SYSTEM_INCHI:BOOL=TRUE
        -DPHP_BINDINGS:BOOL=FALSE
        # Ruby bindings currently fail to build with ruby:2.2
        -DRUBY_BINDINGS:BOOL=FALSE
        -DWITH_INCHI:BOOL=TRUE
        $(cmake_build doc DOCS)
        $(cmake_build GUI)
        $(cmake_enable OPENMP)
        $(cmake_disable_find gui wxWidgets)
        $(cmake_option java JAVA_BINDINGS)
        $(cmake_option perl PERL_BINDINGS)
        $(cmake_option python PYTHON_BINDINGS)
        $(cmake_option R R_BINDINGS)
        $(cmake_with COORDGEN)
        $(option json -DOPENBABEL_USE_SYSTEM_RAPIDJSON:BOOL=TRUE '')
        $(cmake_with JSON)
        $(cmake_with maestro MAEPARSER)
        $(expecting_tests -DENABLE_TESTS:BOOL=TRUE -DENABLE_TESTS:BOOL=FALSE)
    )

    if option java || option perl || option python ; then
        cmakeparams+=( -DRUN_SWIG:BOOL=TRUE )
    fi

    ecmake "${cmakeparams[@]}"
}

src_compile() {
    default

    option doc && emake docs
}

src_install() {
    cmake_src_install

    local host=$(exhost --target) arch_dependent_alternatives=() other_alternatives=()
    local binaries=(
        obabel obconformer obdistgen obenergy obfit obfitall obgen
        obgrep obminimize obmm obprobe obprop obrms obrotamer obrotate
        obspectrophore obsym obtautomer obthermo roundtrip
    )
    local man_pages=(
        obabel obchiral obconformer obdistgen obenergy obfit obgen obgrep obgui
        obminimize obprobe obprop obrms obrotamer obrotate obspectrophore obsym
        obtautomer obthermo roundtrip
    )

    for bin in ${binaries[@]} ; do
        arch_dependent_alternatives+=( /usr/${host}/bin/${bin} ${bin}-${SLOT} )
    done
    for man in ${man_pages[@]} ; do
        other_alternatives+=( /usr/share/man/man1/${man}.1 ${man}-${SLOT}.1 )
    done

    arch_dependent_alternatives+=(
        /usr/${host}/lib/lib${PN}.so lib${PN}-${SLOT}.so
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"

    if option doc; then
        edo pushd "${CMAKE_SOURCE}"/doc/API/
        insinto /usr/share/doc/${PNVR}
        doins -r html
        edo popd
    fi
}

